#pragma checksum "C:\Users\Dhika\source\repos\MattelProject\MattelProject\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0043e49ccf299af52aedf94dd3b664339cc3cf81"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Dhika\source\repos\MattelProject\MattelProject\Views\_ViewImports.cshtml"
using MattelProject;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Dhika\source\repos\MattelProject\MattelProject\Views\_ViewImports.cshtml"
using MattelProject.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0043e49ccf299af52aedf94dd3b664339cc3cf81", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"55d9713b6aa891aeaea218600911b2f0c20692c3", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\Users\Dhika\source\repos\MattelProject\MattelProject\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Home Page";

    var test = ViewBag.testCount;

    var data = ViewBag.Message;

    //var oeeData = ViewBag.oeeData;

    var employeeStatistics = ViewBag.EmployeeData;

#line default
#line hidden
#nullable disable
            WriteLiteral(@"

<style>
    #a-drag-1, #a-drag-2, #a-drag-3 {
        font-size: 170%;
        width: 100%;
        background-color: #0b0b0d;
        margin: 0.5rem 0 0 0.5rem;
        color: white;
        touch-action: none;
        user-select: none;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        border-radius: 10px;
        font-family: 'Open Sans', sans-serif;
    }

    #a-drag-1 {
    }

    #a-drag-2 {
    }


    #a-drag-4, #a-drag-5 {
        font-size: 170%;
        width: 100%;
        margin: 0.5rem 0 0 0.5rem;
        background-color: #0b0b0d;
        color: white;
        padding: 5px;
        border-radius: 10px;
        font-family: 'Open Sans', sans-serif;
    }



    #b-drag-1 {
        font-size: 130%;
        width: 100%;
        margin: 0.5rem 0 0 0.5rem;
        background-color: #0b0b0d;
        color: white;
        border-radius: 10px;
        padding: 5px;
        font-family: 'Open Sans', sans-serif");
            WriteLiteral(@";
    }

    #b-drag-2 {
        width: 100%;
        margin: 0.5rem 0 0 0.5rem;
        background-color: #0b0b0d;
        color: white;
        border-radius: 10px;
        padding: 5px;
    }

    .carousel-inner .carousel-item {
        transition: -webkit-transform 2s ease;
        transition: transform 2s ease;
        transition: transform 2s ease, -webkit-transform 2s ease;
    }

    #c-drag-1 {
        width: 100%;
        margin: 0.5rem 0 0 0.5rem;
        background-color: #0b0b0d;
        color: white;
        padding: 5px;
        border-radius: 10px;
    }

    #c-drag-2, #c-drag-3 {
        font-size: 150%;
        width: 100%;
        margin: 0.5rem 0 0 0.5rem;
        background-color: #0b0b0d;
        color: white;
        border-radius: 10px;
        padding: 5px;
        font-family: 'Open Sans', sans-serif;
    }

    .bg-gradient {
        background-color: #0b0b0d;
        /*background-image: linear-gradient(147deg, #0b0b0d 70%, #434343 30%);*/
");
            WriteLiteral(@"    }

    .ridge-border {
        border-style: ridge;
        background-color: #535251;
        border-color: #c4c5ce;
    }



    .gauge-container.two > .gauge > .value {
        /*stroke: orange;*/
        stroke-dasharray: none;
        stroke-width: 13;
    }

    .carousel-control {
        top: 50%;
        bottom: 50%;
    }

    .padding-0 {
        padding-right: 0.25rem;
        padding-left: 0.25rem;
    }

    #my-div {
        width: auto;
        height: 300px;
        overflow: hidden;
        position: relative;
    }

    #my-iframe {
        position: absolute;
        top: -575px;
        left: -690px;
        width: 1639px;
        height: 820px;
    }

    .carousel-control-prev-icon-custom,
    .carousel-control-next-icon-custom {
        height: 100px;
        width: 100px;
        outline: black;
        background-size: 100%, 100%;
        border-radius: 50%;
        border: 1px solid black;
        background-image: none;
    }
");
            WriteLiteral(@"
        .carousel-control-next-icon-custom:after {
            content: '>';
            font-size: 55px;
            color: #ef4655;
        }

        .carousel-control-prev-icon-custom:after {
            content: '<';
            font-size: 55px;
            color: #ef4655;
        }
</style>


<div class=""row"">

    <div class=""col-xl-2 padding-0"">
        <div id=""a-drag-1"" class=""draggable bg-gradient"">
            <a target=""_blank"" style=""height: 100%; width: 100%"" href=""https://hotelmix.id/weather/cikarang-46315""><img src=""https://w.bookcdn.com/weather/picture/1_46315_1_27_137AE9_215_ffffff_333333_08488D_1_ffffff_333333_0_6.png?scode=124&domid=&anc_id=84700"" style=""width: 100%"" alt=""booked.net"" /></a><!-- weather widget end -->
        </div>
        <div id=""a-drag-2"" class=""draggable bg-gradient"">
            <p class=""text-center""><b>Near Missed</b><br>This Week<br><b>150</b><br><small>report</small><br><b>80%</b><br><small>YTD</small></p>
        </div>
        <div id=""a");
            WriteLiteral(@"-drag-3"" class=""draggable bg-gradient text-center"">
            <img src=""/img/car-img/toycar.png"" style=""width:40%;"" class=""rounded mx-auto img-fluid d-block"" />
            # cars this week
            <b>175.170</b> <br />
            # target this week
            <b>250.000</b>
        </div>
        <div id=""a-drag-4"" class=""draggable bg-gradient text-center"">
            <p> <b>Efficiency</b></p>

");
            WriteLiteral(@"            <table class=""table table-bordered ridge-border"" style=""color:white;"">
                <thead>
                    <tr>
                        <th scope=""col""><span style=""font-weight: lighter;"">2020</span></th>
                        <th scope=""col""><span style=""font-weight: lighter;"">YTD 2021</span></th>
                        <th scope=""col""><span style=""font-weight: lighter;"">WTD</span></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><span style=""font-weight: bolder;"">80%</span></td>
                        <td><span style=""font-weight: bolder;"">80%</span></td>
                        <td><span style=""font-weight: bolder;"">80%</span></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div id=""a-drag-5"" class=""draggable bg-gradient text-center"">
            <p><b>LBO</b></p>
            <table class=""table table-bordered ridge-border"" styl");
            WriteLiteral(@"e=""color:white;"">
                <thead>
                    <tr>
                        <th scope=""col""><span style=""font-weight: lighter;"">2020</span></th>
                        <th scope=""col""><span style=""font-weight: lighter;"">YTD 2021</span></th>
                        <th scope=""col""><span style=""font-weight: lighter;"">WTD</span></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><span style=""font-weight: bolder;"">80%</span></td>
                        <td><span style=""font-weight: bolder;"">80%</span></td>
                        <td><span style=""font-weight: bolder;"">80%</span></td>
                    </tr>
                    <tr>
                        <td><span style=""font-weight: bolder;"">pom</span></td>
                        <td><span style=""font-weight: bolder;"">ppm</span></td>
                        <td><span style=""font-weight: bolder;"">ppmi</span></td>
                    </tr>
    ");
            WriteLiteral(@"            </tbody>
            </table>

        </div>
    </div>

    <div class=""col-xl-6 padding-0"">
        <div id=""b-drag-1"" class=""draggable bg-gradient"">
            <p><b>OEE</b></p>

            <div class=""row text-center"">
                <div class=""col"">
                    <a href=""https://azea1mbms001pv.corp.mattel.com/#%2FServer%201%2FGRAFIK%2FMAIN%20HOME%2FMAIN%20HOME"" target=""_blank""> <div id=""gauge1"" class=""gauge-container two mx-auto"" style=""width:110%""></div></a>
                    <div class="""">PIM</div>
                </div>
                <div class=""col"">
                    <a href=""https://azea1mbms001pv.corp.mattel.com/#%2FServer%201%2FGRAFIK%2FMAIN%20HOME%2FMAIN%20HOME"" target=""_blank"">
                        <div id=""gauge2"" class=""gauge-container two mx-auto"" style=""width: 110%""></div>
                    </a>
                    <div class="""">Die Cast</div>
                </div>
                <div class=""col"">
                    <a data-toggle=");
            WriteLiteral(@"""modal"" data-target=""#omcModal"">
                        <div id=""gauge3"" class=""gauge-container two mx-auto"" style=""width: 110%""></div>
                    </a>
                    <div class="""">OMC</div>
                </div>
                <div class=""col"">
                    <a data-toggle=""modal"" data-target=""#tampoModal"">
                        <div id=""gauge4"" class=""gauge-container two mx-auto"" style=""width: 110%""></div>
                    </a>
                    <div class="""">TAMPO</div>
                </div>
                <div class=""col"">
                    <a data-toggle=""modal"" data-target=""#espModal"">
                        <div id=""gauge5"" class=""gauge-container two mx-auto"" style=""width: 110%""></div>
                    </a>
                    <div class="""">ESP</div>
                </div>
                <div class=""col"">
                    <a data-toggle=""modal"" data-target=""#barbellModal"">
                        <div id=""gauge6"" class=""gauge-container two mx");
            WriteLiteral(@"-auto"" style=""width: 110%""></div>
                    </a>
                    <div class="""">BARBELL</div>
                </div>
            </div>
        </div>





        <div id=""b-drag-2"" class=""draggable bg-gradient"">
            <div id=""carouselExampleControls"" class=""carousel slide"" data-ride=""carousel"" pause=""hover"" data-interval=""20000"">
                <div class=""carousel-inner"">
");
            WriteLiteral(@"

                    <div class=""carousel-item active"">
                            <iframe class=""d-block w-100"" height=""850"" width=""300"" src=""https://azea1mbms001pv.corp.mattel.com/#%2FServer%201%2FGRAFIK%2FMAIN%20HOME%2FMAIN%20HOME"" allowfullscreen></iframe>
                        </div>
                        <div class=""carousel-item"">
                            <iframe class=""d-block w-100"" height=""850"" width=""300"" src=""https://azea1mbms001pv.corp.mattel.com/#%2FServer%201%2FGRAFIK%2FMAIN%20HOME%2FMAIN%20HOME%20High%20Interface"" allowfullscreen></iframe>
                        </div>
                        <div class=""carousel-item"">
                            <iframe class=""d-block w-100"" height=""850"" width=""300"" src=""https://azea1mbms001pv/#%2FServer%201%2FGRAFIK%2FPLANT%20ROOM%2FUTILITY"" allowfullscreen></iframe>
                        </div>
                        <div class=""carousel-item"">
                            <iframe class=""d-block w-100"" height=""850"" width=""300"" src=""");
            WriteLiteral(@"https://azea1mbms001pv/#%2FServer%201%2FGRAFIK%2FBD%202%2FBD2%20LAYOUT%20AHU"" allowfullscreen></iframe>
                        </div>
                        <div class=""carousel-item"">
                            <iframe class=""d-block w-100"" height=""850"" width=""300"" src=""https://azea1mbms001pv/#%2FServer%201%2FGRAFIK%2FBD1%2FBD1%20LIGH%20LAYOUT"" allowfullscreen></iframe>
                        </div>
                        <div class=""carousel-item"">
                            <iframe class=""d-block w-100"" height=""850"" width=""300"" src=""https://azea1mbms001pv/#%2FServer%201%2FGRAFIK%2FBD1%2FBD1%20AHU%201-9"" allowfullscreen></iframe>
                        </div>
                </div>
                <a class=""carousel-control-prev carousel-control"" href=""#carouselExampleControls"" role=""button"" data-slide=""prev"">
                    <span class=""carousel-control-prev-icon-custom"" aria-hidden=""true""></span>
                    <span class=""sr-only"">Previous</span>
                </a>
      ");
            WriteLiteral(@"          <a class=""carousel-control-next carousel-control"" href=""#carouselExampleControls"" role=""button"" data-slide=""next"">
                    <span class=""carousel-control-next-icon-custom"" aria-hidden=""true""></span>
                    <span class=""sr-only"">Next</span>
                </a>
            </div>
        </div>


    </div>


    <div class=""col-xl-4 padding-0"">
        <div id=""c-drag-1"" class=""draggable bg-gradient"">
            <div id=""microsoftStreamCarousel"" class=""carousel slide"" data-ride=""carousel"" data-pause=""hover"" data-interval=""1800000"">
                <div class=""carousel-inner"">
");
            WriteLiteral(@"                    <div class=""carousel-item active"">
                            <iframe width=""1000"" height=""560"" src=""https://web.microsoftstream.com/embed/video/8a856711-1eb7-45ee-80df-2a012370ec26?autoplay=false&amp;showinfo=true"" title=""YouTube video player"" frameborder=""0"" allow=""accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"" allowfullscreen></iframe>
                        </div>
                        <div class=""carousel-item"">
                            <iframe width=""1000"" height=""560"" src=""https://web.microsoftstream.com/embed/video/8a856711-1eb7-45ee-80df-2a012370ec26?autoplay=false&amp;showinfo=true"" title=""YouTube video player"" frameborder=""0"" allow=""accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"" allowfullscreen></iframe>
                        </div>
                        <div class=""carousel-item"">
                            <iframe width=""1000"" height=""560"" src=""https://web.microsoftstream.c");
            WriteLiteral(@"om/embed/video/8a856711-1eb7-45ee-80df-2a012370ec26?autoplay=false&amp;showinfo=true"" title=""YouTube video player"" frameborder=""0"" allow=""accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"" allowfullscreen></iframe>
                        </div>
                </div>
                <a class=""carousel-control-prev carousel-control"" href=""#microsoftStreamCarousel"" role=""button"" data-slide=""prev"">
                    <span class=""carousel-control-prev-icon"" aria-hidden=""true""></span>
                    <span class=""sr-only"">Previous</span>
                </a>
                <a class=""carousel-control-next carousel-control"" href=""#microsoftStreamCarousel"" role=""button"" data-slide=""next"">
                    <span class=""carousel-control-next-icon"" aria-hidden=""true""></span>
                    <span class=""sr-only"">Next</span>
                </a>
            </div>

");
            WriteLiteral(@"        </div>
        <div id=""c-drag-2"" class=""draggable bg-gradient"">
            <p><b>Energy</b></p>
            <div id=""my-div"">
                <iframe src=""https://azea1mbms001pv.corp.mattel.com/#%2FServer%201%2FGRAFIK%2FMAIN%20HOME%2FMAIN%20HOME"" id=""my-iframe"" scrolling=""no""></iframe>
            </div>




        </div>
        <div id=""c-drag-3"" class=""draggable bg-gradient container-fluid"">

            <div class=""row "">
                <div class=""col-md-8"">
                    <p><b>People</b></p>
                    <div class=""row"">
                        <div class=""col-md-6"">

                            <div class=""row text-center"">
                                <div class=""col"">
                                    <div class=""d-flex flex-column"">
                                        <div class=""p-2""><i class=""fas fa-users fa-3x""></i></div>
                                        <div class=""p-2""> 817</div>
                                    </div>
");
            WriteLiteral(@"                                </div>
                                <div class=""col"">
                                    <div class=""d-flex flex-column"">
                                        <div class=""p-2""><i class=""fas fa-male fa-3x"" style=""color: #0000FF;""></i></div>
                                        <div class=""p-2""> 817</div>
                                    </div>
");
            WriteLiteral(@"                                </div>
                                <div class=""col"">
                                    <div class=""d-flex flex-column"">
                                        <div class=""p-2"">
                                            <i class=""fas fa-female fa-3x"" style=""color: #FFC0CB;""></i>
                                        </div>
                                        <div class=""p-2"">
                                            25
                                        </div>
                                    </div>
");
            WriteLiteral(@"                                </div>
                            </div>

                        </div>


                        <div class=""col-md-6"">
                            <table class=""table table-dark"">
                                <tbody>
                                    <tr>
                                        <th>Pregnant</th>
                                        <td>25</td>

                                    </tr>
                                    <tr>
                                        <th>Maternity</th>
                                        <td>25</td>

                                    </tr>
                                    <tr>
                                        <th>Staff</th>
                                        <td>25</td>

                                    </tr>
                                    <tr>
                                        <th>Non-Staff</th>
                                        <td>25</td>

       ");
            WriteLiteral(@"                             </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <div class=""col-md-4"">
                    <div id=""piechart"" style=""color:#FFFFFF;""></div>

                </div>
            </div>
        </div>
    </div>
</div>

");
            WriteLiteral(@"
<!-- Modal Barbell -->
<div class=""modal fade"" id=""barbellModal"" tabindex=""-1"" role=""dialog"" aria-labelledby=""exampleModalCenterTitle"" aria-hidden=""true"">
    <div class=""modal-dialog modal-dialog-centered modal-lg"" role=""document"">
        <div class=""modal-content"">
            <div class=""modal-header"">
                <h5 class=""modal-title"" id=""exampleModalLabel"">OEE Barbell</h5>
                <button type=""button"" class=""close"" data-dismiss=""modal"" aria-label=""Close"">
                    <span aria-hidden=""true"">&times;</span>
                </button>
            </div>
            <div class=""modal-body"">

                <div class='tableauPlaceholder' id='viz1619065240629' style='position: relative'>
                    <noscript><a href='#'><img alt='Dashboard 1 ' src='https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;OE&#47;OEEDonwtimeAreaBarbel&#47;Dashboard1&#47;1_rss.png' style='border: none' /></a></noscript><object class='tableauViz' style='display:none;'>
       ");
            WriteLiteral(@"                 <param name='host_url' value='https%3A%2F%2Fpublic.tableau.com%2F' />
                        <param name='embed_code_version' value='3' />
                        <param name='site_root' value='' />
                        <param name='name' value='OEEDonwtimeAreaBarbel&#47;Dashboard1' />
                        <param name='tabs' value='no' />
                        <param name='toolbar' value='yes' />
                        <param name='static_image' value='https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;OE&#47;OEEDonwtimeAreaBarbel&#47;Dashboard1&#47;1.png' />
                        <param name='animate_transition' value='yes' />
                        <param name='display_static_image' value='yes' />
                        <param name='display_spinner' value='yes' />
                        <param name='display_overlay' value='yes' />
                        <param name='display_count' value='yes' />
                        <param name='language' value='en' ");
            WriteLiteral(@"/>
                    </object>
                </div>
                <script type='text/javascript'>
                    var divElement = document.getElementById('viz1619065240629');
                    var vizElement = divElement.getElementsByTagName('object')[0];
                    if (divElement.offsetWidth > 800) {
                        vizElement.style.width = '5760px';
                        vizElement.style.height = '2187px';
                    } else if (divElement.offsetWidth > 500) {
                        vizElement.style.width = '5760px';
                        vizElement.style.height = '2187px';
                    } else {
                        vizElement.style.width = '100%';
                        vizElement.style.height = '3477px';
                    }
                    var scriptElement = document.createElement('script');
                    scriptElement.src = 'https://public.tableau.com/javascripts/api/viz_v1.js';
                    vizElement.parentNode.");
            WriteLiteral(@"insertBefore(scriptElement, vizElement);
                </script>
            </div>
            <div class=""modal-footer"">
                <button type=""button"" class=""btn btn-secondary"" data-dismiss=""modal"">Close</button>
            </div>


        </div>
    </div>
</div>

<!-- Modal Tampo -->
<div class=""modal fade"" id=""tampoModal"" tabindex=""-1"" role=""dialog"" aria-labelledby=""exampleModalCenterTitle"" aria-hidden=""true"">
    <div class=""modal-dialog modal-dialog-centered modal-lg"" role=""document"">
        <div class=""modal-content"">
            <div class=""modal-header"">
                <h5 class=""modal-title"" id=""exampleModalLabel"">OEE Tampo</h5>
                <button type=""button"" class=""close"" data-dismiss=""modal"" aria-label=""Close"">
                    <span aria-hidden=""true"">&times;</span>
                </button>
            </div>
            <div class=""modal-body"">
                <div class='tableauPlaceholder' id='viz1619065273816' style='position: relative'>
     ");
            WriteLiteral(@"               <noscript><a href='#'><img alt='Dashboard 1 ' src='https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;OE&#47;OEEDonwtimeAreaTampo&#47;Dashboard1&#47;1_rss.png' style='border: none' /></a></noscript><object class='tableauViz' style='display:none;'>
                        <param name='host_url' value='https%3A%2F%2Fpublic.tableau.com%2F' />
                        <param name='embed_code_version' value='3' />
                        <param name='site_root'");
            BeginWriteAttribute("value", " value=\'", 25865, "\'", 25873, 0);
            EndWriteAttribute();
            WriteLiteral(@" />
                        <param name='name' value='OEEDonwtimeAreaTampo&#47;Dashboard1' />
                        <param name='tabs' value='no' />
                        <param name='toolbar' value='yes' />
                        <param name='static_image' value='https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;OE&#47;OEEDonwtimeAreaTampo&#47;Dashboard1&#47;1.png' />
                        <param name='animate_transition' value='yes' />
                        <param name='display_static_image' value='yes' />
                        <param name='display_spinner' value='yes' />
                        <param name='display_overlay' value='yes' />
                        <param name='display_count' value='yes' />
                        <param name='language' value='en' />
                    </object>
                </div>
                <script type='text/javascript'>
                    var divElement = document.getElementById('viz1619065273816');
                    var vi");
            WriteLiteral(@"zElement = divElement.getElementsByTagName('object')[0];
                    if (divElement.offsetWidth > 800) {
                        vizElement.style.width = '5760px';
                        vizElement.style.height = '2187px';
                    } else if (divElement.offsetWidth > 500) {
                        vizElement.style.width = '5760px';
                        vizElement.style.height = '2187px';
                    } else {
                        vizElement.style.width = '100%';
                        vizElement.style.height = '3477px';
                    }
                    var scriptElement = document.createElement('script');
                    scriptElement.src = 'https://public.tableau.com/javascripts/api/viz_v1.js';
                    vizElement.parentNode.insertBefore(scriptElement, vizElement);
                </script>
            </div>
            <div class=""modal-footer"">
                <button type=""button"" class=""btn btn-secondary"" data-dismiss=""modal"">Clo");
            WriteLiteral(@"se</button>
            </div>


        </div>
    </div>
</div>


<!-- Modal OMC -->
<div class=""modal fade"" id=""omcModal"" tabindex=""-1"" role=""dialog"" aria-labelledby=""exampleModalCenterTitle"" aria-hidden=""true"">
    <div class=""modal-dialog modal-dialog-centered modal-lg"" role=""document"">
        <div class=""modal-content"">
            <div class=""modal-header"">
                <h5 class=""modal-title"" id=""exampleModalLabel"">OEE OMC</h5>
                <button type=""button"" class=""close"" data-dismiss=""modal"" aria-label=""Close"">
                    <span aria-hidden=""true"">&times;</span>
                </button>
            </div>
            <div class=""modal-body"">
                <div class='tableauPlaceholder' id='viz1619065309639' style='position: relative'>
                    <noscript><a href='#'><img alt='Dashboard 1 ' src='https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;OE&#47;OEEDonwtimeAreaOMC&#47;Dashboard1&#47;1_rss.png' style='border: none' /></a></noscript>");
            WriteLiteral(@"<object class='tableauViz' style='display:none;'>
                        <param name='host_url' value='https%3A%2F%2Fpublic.tableau.com%2F' />
                        <param name='embed_code_version' value='3' />
                        <param name='site_root'");
            BeginWriteAttribute("value", " value=\'", 29210, "\'", 29218, 0);
            EndWriteAttribute();
            WriteLiteral(@" />
                        <param name='name' value='OEEDonwtimeAreaOMC&#47;Dashboard1' />
                        <param name='tabs' value='no' />
                        <param name='toolbar' value='yes' />
                        <param name='static_image' value='https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;OE&#47;OEEDonwtimeAreaOMC&#47;Dashboard1&#47;1.png' />
                        <param name='animate_transition' value='yes' />
                        <param name='display_static_image' value='yes' />
                        <param name='display_spinner' value='yes' />
                        <param name='display_overlay' value='yes' />
                        <param name='display_count' value='yes' />
                        <param name='language' value='en' />
                    </object>
                </div>
                <script type='text/javascript'>
                    var divElement = document.getElementById('viz1619065309639');
                    var vizEle");
            WriteLiteral(@"ment = divElement.getElementsByTagName('object')[0];
                    if (divElement.offsetWidth > 800) {
                        vizElement.style.width = '5760px';
                        vizElement.style.height = '2187px';
                    } else if (divElement.offsetWidth > 500) {
                        vizElement.style.width = '5760px';
                        vizElement.style.height = '2187px';
                    } else {
                        vizElement.style.width = '100%';
                        vizElement.style.height = '3477px';
                    }
                    var scriptElement = document.createElement('script');
                    scriptElement.src = 'https://public.tableau.com/javascripts/api/viz_v1.js';
                    vizElement.parentNode.insertBefore(scriptElement, vizElement);
                </script>
            </div>
            <div class=""modal-footer"">
                <button type=""button"" class=""btn btn-secondary"" data-dismiss=""modal"">Close</");
            WriteLiteral(@"button>
            </div>


        </div>
    </div>
</div>

<!-- Modal ESP -->
<div class=""modal fade"" id=""espModal"" tabindex=""-1"" role=""dialog"" aria-labelledby=""exampleModalCenterTitle"" aria-hidden=""true"">
    <div class=""modal-dialog modal-dialog-centered modal-lg"" style=""width:1920px;"" role=""document"">
        <div class=""modal-content"">
            <div class=""modal-header"">
                <h5 class=""modal-title"" id=""exampleModalLabel"">OEE ESP</h5>
                <button type=""button"" class=""close"" data-dismiss=""modal"" aria-label=""Close"">
                    <span aria-hidden=""true"">&times;</span>
                </button>
            </div>
            <div class=""modal-body"">
                <div class='tableauPlaceholder' id='viz1618891082392' style='position: relative'><noscript><a href='#'><img alt='Dashboard 1 ' src='https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;OE&#47;OEEDonwtimeAreaOMC&#47;Dashboard1&#47;1_rss.png' style='border: none' /></a></noscript><objec");
            WriteLiteral("t class=\'tableauViz\' style=\'display:none;\'><param name=\'host_url\' value=\'https%3A%2F%2Fpublic.tableau.com%2F\' /> <param name=\'embed_code_version\' value=\'3\' /> <param name=\'site_root\'");
            BeginWriteAttribute("value", " value=\'", 32473, "\'", 32481, 0);
            EndWriteAttribute();
            WriteLiteral(@" /><param name='name' value='OEEDonwtimeAreaOMC&#47;Dashboard1' /><param name='tabs' value='no' /><param name='toolbar' value='yes' /><param name='static_image' value='https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;OE&#47;OEEDonwtimeAreaOMC&#47;Dashboard1&#47;1.png' /> <param name='animate_transition' value='yes' /><param name='display_static_image' value='yes' /><param name='display_spinner' value='yes' /><param name='display_overlay' value='yes' /><param name='display_count' value='yes' /><param name='language' value='en' /><param name='filter' value='publish=yes' /></object></div>
                <script type='text/javascript'>var divElement = document.getElementById('viz1618891082392'); var vizElement = divElement.getElementsByTagName('object')[0]; if (divElement.offsetWidth > 800) { vizElement.style.width = '5760px'; vizElement.style.height = '2187px'; } else if (divElement.offsetWidth > 500) { vizElement.style.width = '5760px'; vizElement.style.height = '2187px'; } else { vizElement.style");
            WriteLiteral(@".width = '100%'; vizElement.style.height = '3477px'; } var scriptElement = document.createElement('script'); scriptElement.src = 'https://public.tableau.com/javascripts/api/viz_v1.js'; vizElement.parentNode.insertBefore(scriptElement, vizElement);</script>
            </div>
            <div class=""modal-footer"">
                <button type=""button"" class=""btn btn-secondary"" data-dismiss=""modal"">Close</button>
            </div>


        </div>
    </div>
</div>




<script src=""https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"" type=""text/javascript""></script>
<script>
    //$('video').on('play', function (e) {
    //    $(""#microsoftStreamCarousel"").carousel('pause');
    //});
    //$('video').on('stop pause ended', function (e) {
    //    $(""#microsoftStreamCarousel"").carousel();
    //});

    //$(""#microsoftStreamCarousel"").carousel({ interval: false }); // this prevents the auto-loop
    //var videos = document.querySelectorAll(""video.d-block"");
    //videos.f");
            WriteLiteral(@"orEach(function (e) {
    //    e.addEventListener('ended', myHandler, false);
    //});

    //function myHandler(e) {
    //    $(""#mediaCarousel"").carousel('next');
    //}


    $('#energy-gauge').load('https://localhost:44362/ #b-drag-1');
    //gauge from google
    google.charts.load('current', { 'packages': ['gauge', 'corechart'] });
    google.charts.setOnLoadCallback(drawPieChart);



    function drawPieChart() {
        var data = google.visualization.arrayToDataTable([
            ['Employee', 'Employee Non Staff'],
            ['S', 12],
            ['M', 12],
            ['T', 12]
        ]);

        var options = {
            title: 'Employee',
            titleTextStyle: {
                color: '#FFFFFF'
            },
            legend: 'none',
            backgroundColor: 'transparent',
            width: 500,
            height: 350
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart");
            WriteLiteral(@".draw(data, options);
    }



    function assignGauge(gauge, id, val, color) {
        gauge = Gauge(
            document.getElementById(id),
            {
                min: -100,
                max: 100,
                dialStartAngle: 180,
                dialEndAngle: 0,
                value: val,
                gaugeClass: ""gauge"",
                viewBox: ""0 0 100 57"",
                label: function (value) {
                    return (Math.round(value * 100) / 100).toFixed(1) + ""%"";
                },
                color: function (value) {
                    if (value < 20) {
                        return ""#f7aa38"";
                    } else if (value < 40) {
                        return ""#f7aa38"";
                    } else if (value < 60) {
                        return ""#f7aa38"";
                    } else {
                        return ""#f7aa38"";
                    }
                }
            }
        );
    }

    //var energyGauge1 = as");
            WriteLiteral(@"signGauge(energyGauge1, ""energyGauge1"", 0, ""#f7aa38"");
    //var energyGauge2 = assignGauge(energyGauge2, ""energyGauge2"", 0, ""#f7aa38"");
    //var energyGauge3 = assignGauge(energyGauge3, ""energyGauge3"", 0, ""#f7aa38"");
    //var energyGauge4 = assignGauge(energyGauge4, ""energyGauge4"", 0,""#f7aa38"");

    var gauge1 = assignGauge(gauge1, ""gauge1"", 0.83 * 100, ""#ef4655"");
    var gauge2 = assignGauge(gauge2, ""gauge2"", 83.5, ""#ef4655"");
    var gauge3 = assignGauge(gauge3, ""gauge3"", 70.5, ""#ef4655"");
    var gauge4 = assignGauge(gauge4, ""gauge4"", 60.5, ""#ef4655"");
    var gauge5 = assignGauge(gauge5, ""gauge5"", 80.5, ""#ef4655"");
    var gauge6 = assignGauge(gauge6, ""gauge6"", 70.5, ""#ef4655"");


    !function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = 'https://weatherwidget.io/js/widget.min.js'; fjs.parentNode.insertBefore(js, fjs); } }(document, 'script', 'weatherwidget-io-js');

    function dragMoveListener(ev");
            WriteLiteral(@"ent) {
        var target = event.target
        // keep the dragged position in the data-x/data-y attributes
        var x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx
        var y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy

        // translate the element
        target.style.webkitTransform =
            target.style.transform =
            'translate(' + x + 'px, ' + y + 'px)'

        // update the posiion attributes
        target.setAttribute('data-x', x)
        target.setAttribute('data-y', y)
    }

    // this function is used later in the resizing and gesture demos
    window.dragMoveListener = dragMoveListener



</script>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
