﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MattelProject.Models
{
    public class AddPageModel
    {
        public int AddPageId { get; set; }
        public string AddPageLink { get; set; }

        public string AddPageLabel { get; set; }
    }
}
