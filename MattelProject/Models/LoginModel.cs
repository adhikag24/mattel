﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MattelProject.Models
{
    public class LoginModel
    {
        public int admin_id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }
}
