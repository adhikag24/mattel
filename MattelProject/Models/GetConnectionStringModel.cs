﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace MattelProject.Models
{
    public class GetConnectionStringModel
    {
    

        public string GetConnectionString()
        {
            var configuration = GetConfiguration();
            string conString = configuration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;

            return conString;
        }
        public IConfigurationRoot GetConfiguration()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            return builder.Build();
        }
    }

    
}
