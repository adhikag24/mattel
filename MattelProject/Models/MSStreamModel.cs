﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MattelProject.Models
{
    public class MSStreamModel
    {
        public int MSStreamId { get; set; }
        public string MSSStreamLink { get; set; }
    }
}
