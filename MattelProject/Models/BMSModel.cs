﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MattelProject.Models
{
    public class BMSModel
    {
        public int BMSId { get; set; }
        public string BMSLink { get; set; }
    }
}
