﻿
using System.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using MySql.Data.MySqlClient;
using MattelProject.Models;
using System.Configuration;
using System.Data.SqlClient;

namespace MattelProject.Controllers


{
    public class AdminController : Controller
    {
        private IHostEnvironment _env;

        public AdminController(IHostEnvironment env)
        {
            _env = env;
        }
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult Login()
        {

            return View();
        }

        public ActionResult Form()
        {

            return View();
        }

        public ActionResult BMS()
        {
            List<BMSModel> listData = this.getBMSData();
            ViewBag.Data = listData;
            return View();
        }

        public ActionResult MSStream()
        {
            List<MSStreamModel> listData = this.getMsStreamData();
            ViewBag.Data = listData;
            return View();
        }

        public ActionResult AddPage()
        {
            
            List<AddPageModel> listData = this.geAddPageData();
            ViewBag.Data = listData;

            return View();
        }

        public ActionResult AdminAccount()
        {
            
            List<LoginModel> listData = this.getAdminAccountData();
            ViewBag.Data = listData;
            return View();
        }

        [HttpPost]
        public ActionResult CheckLogin(LoginModel data)
        {
            var userId = 0;
            GetConnectionStringModel connString = new GetConnectionStringModel();
            using (SqlConnection conn = new SqlConnection(connString.GetConnectionString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("select admin_id from Admins  where username='" + data.username + "'  AND password= '" + data.password + "'", conn);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        userId = int.Parse(reader["admin_id"].ToString());
                    }
                }
              
            }

            if(userId > 0)
            {
                return RedirectToAction("Index");
            }
            else
            {

                return RedirectToAction("Login");
            }


        }



        [HttpPost]
        public ActionResult AddPageInsert(AddPageModel data)
        {

            var result = 0;
            GetConnectionStringModel connString = new GetConnectionStringModel();
            using (SqlConnection conn = new SqlConnection(connString.GetConnectionString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("Insert Into additionalPage(additionalPage_header, additionalPage_link)Values('" + data.AddPageLabel + "','" + data.AddPageLink + "')", conn);
                result = cmd.ExecuteNonQuery();
            }


            return RedirectToAction("AddPage");
        }

        [HttpPost]
        public ActionResult AdminACCInsert(LoginModel data)
        {

            var result = 0;
            GetConnectionStringModel connString = new GetConnectionStringModel();
            using (SqlConnection conn = new SqlConnection(connString.GetConnectionString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("Insert Into Admins(username, password)Values('" + data.username + "','" + data.password + "')", conn);
                result = cmd.ExecuteNonQuery();
            }


            return RedirectToAction("AdminAccount");
        }
        

        [HttpPost]
        public ActionResult MSStreamInsert(MSStreamModel data)
        {

            var result = 0;
            GetConnectionStringModel connString = new GetConnectionStringModel();
            using (SqlConnection conn = new SqlConnection(connString.GetConnectionString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("Insert Into msStreamData(msstreamLink)Values('" + data.MSSStreamLink + "')", conn);
                result = cmd.ExecuteNonQuery();
            }


            return RedirectToAction("MSStream");
        }
        [HttpGet]
        public ActionResult deleteMSStreamData(int id)
        {
            SqlDataAdapter adapter = new SqlDataAdapter();
            GetConnectionStringModel connString = new GetConnectionStringModel();
            using (SqlConnection conn = new SqlConnection(connString.GetConnectionString()))
            {

                string deleteData = "DELETE FROM msStreamData WHERE msstreamId='" + id + "'";

                SqlCommand command = new SqlCommand(deleteData, conn);

                conn.Open();

                var result = command.ExecuteNonQuery();
            }

            return RedirectToAction("MSStream");
        }

        [HttpGet]
        public ActionResult deleteAdminAccData(int id)
        {
            SqlDataAdapter adapter = new SqlDataAdapter();
            GetConnectionStringModel connString = new GetConnectionStringModel();
            using (SqlConnection conn = new SqlConnection(connString.GetConnectionString()))
            {

                string deleteData = "DELETE FROM Admins WHERE admin_id='" + id + "'";

                SqlCommand command = new SqlCommand(deleteData, conn);

                conn.Open();

                var result = command.ExecuteNonQuery();
            }

            return RedirectToAction("AdminAccount");
        }
        public List<MSStreamModel> getMsStreamData()
        {
            List<MSStreamModel> list = new List<MSStreamModel>();

            GetConnectionStringModel connString = new GetConnectionStringModel();
            using (SqlConnection conn = new SqlConnection(connString.GetConnectionString()))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("select * from msStreamData", conn);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new MSStreamModel()
                        {
                            MSStreamId = Int32.Parse(reader["msstreamId"].ToString()),
                            MSSStreamLink = reader["msstreamLink"].ToString(),
                        });
                    }
                }
            }
            return list;
        }

        public List<LoginModel> getAdminAccountData()
        {
            List<LoginModel> list = new List<LoginModel>();

            GetConnectionStringModel connString = new GetConnectionStringModel();
            using (SqlConnection conn = new SqlConnection(connString.GetConnectionString()))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("select * from Admins", conn);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new LoginModel()
                        {
                            admin_id = Int32.Parse(reader["admin_id"].ToString()),
                            username = reader["username"].ToString(),
                            password = reader["password"].ToString(),
                        });
                    }
                }
            }
            return list;
        }

        public List<AddPageModel> geAddPageData()
        {
            List<AddPageModel> list = new List<AddPageModel>();

            GetConnectionStringModel connString = new GetConnectionStringModel();
            using (SqlConnection conn = new SqlConnection(connString.GetConnectionString()))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("select * from additionalPage", conn);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new AddPageModel()
                        {
                            AddPageId = Int32.Parse(reader["additionalPage_Id"].ToString()),
                            AddPageLabel = reader["additionalPage_header"].ToString(),
                            AddPageLink = reader["additionalPage_link"].ToString(),
                        });
                    }
                }
            }
            return list;
        }





        [HttpPost]
        public ActionResult BMSInsert(BMSModel data)
        {
            var result = 0;
            GetConnectionStringModel connString = new GetConnectionStringModel();
            using (SqlConnection conn = new SqlConnection(connString.GetConnectionString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("Insert Into bmsData(bmsLink)Values('" + data.BMSLink + "')", conn);
                result = cmd.ExecuteNonQuery();
            }


            return RedirectToAction("BMS");

        }


        [HttpPost]
        public ActionResult Index(IFormFile file)
        {
            var fileName = Path.GetFileName(file.FileName);
            using (var fileStream = new FileStream(Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot\img\car-img", fileName), FileMode.Create, FileAccess.Write))
            {
                file.CopyTo(fileStream);
            }
            insertImage(fileName);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult updateHeaderLabel(String headerLabel)
        {
            GetConnectionStringModel connString = new GetConnectionStringModel();
            using (SqlConnection conn = new SqlConnection(connString.GetConnectionString()))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("UPDATE headerLabel SET header_label = '" + headerLabel + "' WHERE id_header = 1;", conn);
                var result = cmd.ExecuteNonQuery();
            }

            return RedirectToAction("Index");
        }

        public List<BMSModel> getBMSData()
        {
            List<BMSModel> list = new List<BMSModel>();

            GetConnectionStringModel connString = new GetConnectionStringModel();
            using (SqlConnection conn = new SqlConnection(connString.GetConnectionString()))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("select * from bmsData", conn);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new BMSModel()
                        {
                            BMSId = Int32.Parse(reader["bmsId"].ToString()),
                            BMSLink = reader["bmsLink"].ToString(),
                        });
                    }
                }
            }
            return list;
        }
       

        [HttpGet]
        public ActionResult deleteBMSData(int id)
        {
            SqlDataAdapter adapter = new SqlDataAdapter();
            GetConnectionStringModel connString = new GetConnectionStringModel();
            using (SqlConnection conn = new SqlConnection(connString.GetConnectionString()))
            {

                string deleteData = "DELETE FROM bmsData WHERE bmsId='" + id + "'";

                SqlCommand command = new SqlCommand(deleteData, conn);

                conn.Open();

                var result = command.ExecuteNonQuery();
            }

            return RedirectToAction("BMS");
        }

        [HttpGet]
        public ActionResult deleteAddPageData(int id)
        {
            SqlDataAdapter adapter = new SqlDataAdapter();
            GetConnectionStringModel connString = new GetConnectionStringModel();
            using (SqlConnection conn = new SqlConnection(connString.GetConnectionString()))
            {

                string deleteData = "DELETE FROM additionalPage WHERE additionalPage_id='" + id + "'";

                SqlCommand command = new SqlCommand(deleteData, conn);

                conn.Open();

                var result = command.ExecuteNonQuery();
            }

            return RedirectToAction("AddPage");
        }


        public dynamic insertImage(String imageName)
        {

            MySqlDataAdapter adapter = new MySqlDataAdapter();

            using (MySqlConnection conn = new MySqlConnection("server=localhost;port=3306;database=latihanasp;user=root;password=;"))
            {

                string insertData = "Insert Into car_image(car_image)Values('" + imageName + "')";

                MySqlCommand command = new MySqlCommand(insertData, conn);

                conn.Open();

                var result = command.ExecuteNonQuery();

                return result;
            }
        }


    }
}
