﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;

namespace MattelProject.Controllers

{
    public class DataController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            List<DataModel> listData = this.GetUserData();
            ViewBag.Data = listData;
            return View();
        }

        [HttpPost]
        public IActionResult Index(DataModel model)
        {

            var result = this.insertData(model);
            List<DataModel> listData = this.GetUserData();
            ViewBag.Data = listData;
            return View();
        }

        [HttpGet]
        public ActionResult delete(int id)
        {
            
            this.deleteData(id);
            return RedirectToPage("index");
        }

        public dynamic insertData(DataModel model)
        {

            MySqlDataAdapter adapter = new MySqlDataAdapter();

            using (MySqlConnection conn = new MySqlConnection("server=localhost;port=3306;database=latihanasp;user=root;password=;"))
            {

                string insertData = "Insert Into user(first_name,age)Values('"+model.FirstName+"','"+model.Age+"')";
                
                MySqlCommand command = new MySqlCommand(insertData, conn);

                conn.Open();

                var result = command.ExecuteNonQuery();

                return result;
            }
        }

        public dynamic deleteData(int id)
        {
            MySqlDataAdapter adapter = new MySqlDataAdapter();

            using (MySqlConnection conn = new MySqlConnection("server=localhost;port=3306;database=latihanasp;user=root;password=;"))
            {

                string deleteData = "DELETE FROM user WHERE user_id='"+id+"'";

                MySqlCommand command = new MySqlCommand(deleteData, conn);

                conn.Open();

                var result = command.ExecuteNonQuery();

                return result;
            }
        }

        public List<DataModel> GetUserData()
        {
            List<DataModel> list = new List<DataModel>();

            using (MySqlConnection conn = new MySqlConnection("server=localhost;port=3306;database=latihanasp;user=root;password=;"))
            {
                conn.Open();

                MySqlCommand cmd = new MySqlCommand("select * from user", conn);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new DataModel()
                        {
                            DataId = Int32.Parse(reader["user_id"].ToString()),
                            FirstName = reader["first_name"].ToString(),
                            Age = Int32.Parse(reader["age"].ToString()),
                            
                        });
                    }
                }
            }
            return list;
        }
    }
}
