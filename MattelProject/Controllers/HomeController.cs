﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;
using MattelProject.Models;
using System.Configuration;

namespace MattelProject.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;


        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            //ViewBag.testCount = TestSQLData();
            //ViewBag.Message = GetCarData();
            //ViewBag.oeeData = GetOEEData();
            ViewBag.BMSData = getBMSData();
            ViewBag.MSStreamData = getMsStreamData();
            ViewBag.addPageList = this.geAddPageData();
            ViewBag.headerLabel = getHeaderLabel();

            //ViewBag.BMSData = new List<string>();
            //ViewBag.BMSData.Add("https://azea1mbms001pv.corp.mattel.com/#%2FServer%201%2FGRAFIK%2FMAIN%20HOME%2FMAIN%20HOME");
            //ViewBag.MSStreamData = new List<string>();
            //ViewBag.MSStreamData.Add("https://web.microsoftstream.com/embed/video/8a856711-1eb7-45ee-80df-2a012370ec26?autoplay=false&amp;showinfo=true");
            //ViewBag.addPageList = new List<string>();
            //ViewBag.headerLabel = "Hot Wheel";

            ViewBag.testCount = "12";
            ViewBag.Message = "toycar.png";
            return View();
        }
        [HttpGet]
        public ActionResult AddPage(int id)
        {
            ViewBag.headerLabel = getHeaderLabel();
            ViewBag.iframeLink = this.getIframeLink(id);
            ViewBag.addPageList = this.geAddPageData();

            
            return View();
        }

        public IActionResult Privacy()
        {
            ViewBag.addPageList = this.geAddPageData();
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public string getIframeLink(int id)
        {
          
                String link = String.Empty;
                GetConnectionStringModel connString = new GetConnectionStringModel();
                using (SqlConnection conn = new SqlConnection(connString.GetConnectionString()))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("select additionalPage_link FROM additionalPage where additionalPage_id = '" + id + "'", conn);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                        link = reader["additionalPage_link"].ToString();
                        }
                    }
                }
                return link;
           
        }

        public IActionResult OMC()
        {
            ViewBag.addPageList = this.geAddPageData();
            ViewBag.headerLabel = getHeaderLabel();
            return View();
        }

        public IActionResult VUM()
        {
            ViewBag.addPageList = this.geAddPageData();
            ViewBag.headerLabel = getHeaderLabel();
            return View();
        }

        public IActionResult TAMPO()
        {
            ViewBag.addPageList = this.geAddPageData();
            ViewBag.headerLabel = getHeaderLabel();
            return View();
        }

        public IActionResult BARBELL()
        {
            ViewBag.addPageList = this.geAddPageData();
            ViewBag.headerLabel = getHeaderLabel();
            return View();
        }

        public IActionResult DIECAST()
        {
            ViewBag.addPageList = this.geAddPageData();
            ViewBag.headerLabel = getHeaderLabel();
            return View();
        }

        public IActionResult PIM()
        {
            ViewBag.addPageList = this.geAddPageData();
            ViewBag.headerLabel = getHeaderLabel();
            return View();
        }

        public IActionResult ESP()
        {
            ViewBag.addPageList = this.geAddPageData();
            ViewBag.headerLabel = getHeaderLabel();
            return View();
        }

        public IActionResult PLATING()
        {
            ViewBag.addPageList = this.geAddPageData();
            ViewBag.headerLabel = getHeaderLabel();
            return View();
        }

        public IActionResult PACKOUT()
        {
            ViewBag.addPageList = this.geAddPageData();
            ViewBag.headerLabel = getHeaderLabel();
            return View();
        }

        public IActionResult PEOPLE()
        {
            ViewBag.addPageList = this.geAddPageData();
            ViewBag.headerLabel = getHeaderLabel();
            return View();
        }

        public int TestSQLData() {


            SqlConnection connection = new SqlConnection("Server=.;Database=test-db;Trusted_Connection=True");
            int total = 0;
            connection.Open();
            SqlCommand com = new SqlCommand("Select count(*) from [user]", connection);

        
            total = (int) com.ExecuteScalar();
        

            connection.Close();

            return total;
        }

        public Dictionary<string, string> GetOEEData()
        {
            var map = new Dictionary<string, string>();

            using (SqlConnection conn = new SqlConnection("Server=.;Database=test-db;Trusted_Connection=True"))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("SELECT (SELECT AVG(OEE) FROM OEEBarbell WHERE OEE > 0) OEEBarbell, (SELECT AVG(OEE) FROM OEEESP WHERE OEE > 0) OEEESP, (SELECT AVG(F21) FROM OEETampo WHERE F21 > 0) OEETampo, (SELECT AVG(OEE) FROM OEEOMC WHERE OEE > 0) OEEOMC, (SELECT AVG(OEE) FROM OEEPIM WHERE OEE > 0) OEEPIM", conn);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        map["oeeTampo"] = (Convert.ToDouble(reader["oeeTampo"]) * 100).ToString();
                        map["oeeBarbell"] = (Convert.ToDouble(reader["oeeBarbell"]) * 100).ToString();
                        map["oeeESP"] = (Convert.ToDouble(reader["oeeESP"]) * 100).ToString();
                        map["oeeOMC"] = (Convert.ToDouble(reader["oeeOMC"]) * 100).ToString();
                        map["oeePIM"] = (Convert.ToDouble(reader["oeePIM"]) * 100).ToString();
                    }
                }
            }

            return map;
        }

        public List<BMSModel> getBMSData()
        {
            List<BMSModel> list = new List<BMSModel>();

            GetConnectionStringModel connString = new GetConnectionStringModel();
            using (SqlConnection conn = new SqlConnection(connString.GetConnectionString()))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("select * from bmsData", conn);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new BMSModel()
                        {
                            BMSId = Int32.Parse(reader["bmsId"].ToString()),
                            BMSLink = reader["bmsLink"].ToString(),
                        });
                    }
                }
            }
            return list;
        }

        public List<AddPageModel> geAddPageData()
        {
            List<AddPageModel> list = new List<AddPageModel>();

            GetConnectionStringModel connString = new GetConnectionStringModel();
            using (SqlConnection conn = new SqlConnection(connString.GetConnectionString()))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("select * from additionalPage", conn);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new AddPageModel()
                        {
                            AddPageId = Int32.Parse(reader["additionalPage_Id"].ToString()),
                            AddPageLabel = reader["additionalPage_header"].ToString(),
                            AddPageLink = reader["additionalPage_link"].ToString(),
                        });
                    }
                }
            }
            return list;
        }

        public List<MSStreamModel> getMsStreamData()
        {
            List<MSStreamModel> list = new List<MSStreamModel>();

            GetConnectionStringModel connString = new GetConnectionStringModel();
            using (SqlConnection conn = new SqlConnection(connString.GetConnectionString()))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("select * from msStreamData", conn);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new MSStreamModel()
                        {
                            MSStreamId = Int32.Parse(reader["msstreamId"].ToString()),
                            MSSStreamLink = reader["msstreamLink"].ToString(),
                        });
                    }
                }
            }
            return list;
        }

        public String GetCarData()
        {
            String image = String.Empty;

            using (MySqlConnection conn = new MySqlConnection("server=localhost;port=3306;database=latihanasp;user=root;password=;"))
            {
                conn.Open();

                MySqlCommand cmd = new MySqlCommand("select car_image FROM car_image ORDER BY carimage_id DESC LIMIT 1", conn);
 
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        image = reader["car_image"].ToString();
                    }
                }
            }
            return image;
        }

        public String getHeaderLabel()
        {
            String text = String.Empty;

            GetConnectionStringModel connString = new GetConnectionStringModel();
            using (SqlConnection conn = new SqlConnection(connString.GetConnectionString()))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("select header_label from headerLabel", conn);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        text = reader["header_label"].ToString();
                    }
                }
            }
            return text;
        }


    }
}
