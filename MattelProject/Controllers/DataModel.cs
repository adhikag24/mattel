﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MattelProject.Controllers
{
    public class DataModel
    {
        public int DataId { get; set; }
        public string FirstName { get; set; }
        public int Age { get; set; }

    }
}
